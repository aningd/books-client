var containerElement = document.getElementById("container");
var host = "http://localhost:3000";
var logKey;

//hides all pages
function pageHide() {
	document.getElementById("books-page").style.display = "";
	document.getElementById("login-page").style.display = "";
    document.getElementById("details-page").style.display = "";
    document.getElementById("list-page").style.display = "";
}

//shows the login page and loads content
function loadLoginPage() {
	pageHide();
	document.getElementById("login-page").style.display = "inline-block";
	document.getElementById("main-title").innerHTML = "Sign In";
}

//shows the books page
function loadBooksPage() {
	pageHide();
	document.getElementById("main-title").innerHTML = "Books";
	document.getElementById("books-page").style.display = "inline-block";
	
	//get book listings from booksAPI
	httpGet(host + "/books", function(data){
		
		//store JSON data into an array (of objects)
		var listings = JSON.parse(data);
		console.log(listings);
		
		//render the results
        if(listings != "undefined") {
            renderBooks(listings);
        }
		
	});
	
}

//shows the reading list page
function loadListPage() {
	pageHide();
	document.getElementById("main-title").innerHTML = "Reading List";
	document.getElementById("list-page").style.display = "inline-block";
	
	console.log(host + "/readinglist/"+logKey);
	
	//get book listings from booksAPI
	httpGet(host + "/readinglist/"+logKey, function(data){
		
		//store JSON data into an array (of objects)
		var listings = JSON.parse(data);
		console.log(listings);
		
		//render the results
        if(listings != "undefined") {
            renderBooks(listings, "list-listings");
        }
		
	});
	
}

//ISBN search click event
document.getElementById("btn-search").addEventListener("click", function() {
    
    //get the isbn number from the search input box
    var isbn = document.getElementById("search").value;
                                                       
    if(typeof isbn != 'undefined') {
        //get book listings from booksAPI
        httpGet(host + "/books/"+isbn, function(data){

            //store JSON data into an array (of objects)
            var listings = JSON.parse(data);
            console.log(listings);

            //render the results
            if(listings != "undefined") {
                renderBooks([listings]);
            }
        });
    }
});

//details page back button click event
document.getElementById("details-back").addEventListener("click", function() {
    loadBooksPage();
});

//shows the book details page
function loadDetailsPage(book) {
    pageHide();
    document.getElementById("main-title").innerHTML = book.title;
    document.getElementById("details-page").style.display = "inline-block";
    
    //render book details
    document.getElementById("book-details").innerHTML = '<img src="books/'+book.picture+'" /><p></p><div><label>TITLE: </label>'+book.title+'</div> <div><label>AUTHOR: </label>'+book.author+'</div><div><label>YEAR: </label>'+book.year+'</div><div><label>ISBN: </label>'+book.isbn+'</div><div><label>DESCRIPTION: </label>'+book.description+'</div>';
}

//get function
function httpGet(url, callback) {
	var request = new XMLHttpRequest();
	request.open("GET", url, true);
	request.onreadystatechange = function () {
		if(request.readyState == 4 && request.status == 200) {
			callback(request.responseText);
		}
	}
	request.send();
}

//post function
function httpPost(url, data, callback) {
	var request = new XMLHttpRequest();
	request.open("POST", url, true);
	request.setRequestHeader('Content-Type', 'application/json');	//using JSON content type
	request.onreadystatechange = function () {
		if(request.readyState == 4 && request.status == 200) {
			callback(request.responseText);
		}
	}
	request.send(data);
}

//login user click event
document.getElementById("btn-login").addEventListener("click", function() {
	
	//prepare registration data in object
     var logData = {
     	username: document.getElementById("log-username").value,
     	password: document.getElementById("log-password").value
     };
     
     console.log("Logging in with: "+JSON.stringify(logData));
     
     //post the data to the server
     httpPost(host + "/users/login", JSON.stringify(logData), function(e){
     	
     	//notifications
     	if(e == "invalid") {
     		alert("Failed to sign in. Please try again.");
     	} else {
     		loadBooksPage();
     		logKey = e.replace(/['"]+/g, '');
     		console.log("Login successful: " + e);
     	}
     });
});

//register user button click event
document.getElementById("btn-register").addEventListener("click", function() {
	
	//prepare registration data in object
     var regData = {
     	name: document.getElementById("reg-name").value,
     	surname: document.getElementById("reg-surname").value,
     	username: document.getElementById("reg-username").value,
     	password: document.getElementById("reg-password").value
     };
     
     console.log("Registering with: "+JSON.stringify(regData));
     
     //post the data to the server
     httpPost(host + "/users", JSON.stringify(regData), function(e){
     	
     	//notifications
     	if(e == "success") {
     		document.getElementById("form-register").style.display = "none";	//hide registration form
     		alert("Registration successful. Please sign in");
     	} else {
     		alert("Your registration has failed.");
     	}
     });
});

//render books
function renderBooks(object, elem) {
    
	//variable to store html content
	var html = "";
	var to = (typeof elem != 'undefined') ? elem : "listings";
	var mode = (to == "listings") ? "Add to Reading List" : "Remove from list";
	var func = (to == "listings") ? "add" : "remove";
	var view = (to == "listings") ? "bview" : "lview";
	
	//iterate through all books in the array and generate list html
	for(var i = 0; i < object.length; i++) {
		var item = object[i];
		html += '<li><div>'+item.title+' by '+item.author+'</div><img src="books/'+item.picture+'" /><div><button id="'+func+item._id+'">'+mode+'</button><button id="'+view+item._id+'">View</button></div></li>';
	}
    
    //append the html to the unordered list
    
    document.getElementById(to).innerHTML = html;
    
    //attach click events to items
    for(var i = 0; i < object.length; i++) {
        (function () {  //use annonymous self-executing function to avoid non-strict javascript
            var item = object[i];
         
            //reading list button click event
            var entryData = {
            	user: logKey,
            	book: item._id
            };  
            if(to == "listings") { 
	            document.getElementById("add"+item._id).addEventListener("click", function() {
	                          
	                //add to reading list
	                httpPost(host + "/readinglist", JSON.stringify(entryData), function(data){
			
						if(data == "success") {
							//notification
			                alert(item.title+' by '+item.author+" was added to reading your list");
						} else {
							alert(item.title+' by '+item.author+" was not added to reading your list");
						}
						
					});
					
	            });
	            //view button click event
	            document.getElementById("bview"+item._id).addEventListener("click", function() {
	                loadDetailsPage(item);
	            });
            } else {
            	document.getElementById("remove"+item._id).addEventListener("click", function() {
	            	//remove reading list
	                httpPost(host + "/readinglist/remove", JSON.stringify(entryData), function(data){
			
						if(data == "success") {
							//notification
			                alert(item.title+' by '+item.author+" was removed from reading your list");
						} else {
							alert(item.title+' by '+item.author+" was not removed from reading your list");
						}
						
					});
				 });
				 
				 //view button click event
	            document.getElementById("lview"+item._id).addEventListener("click", function() {
	                loadDetailsPage(item);
	            });
            }
         
            
         }());
    }
	
}

loadLoginPage();

//list page back button click event
document.getElementById("reading-list-back").addEventListener("click", function() {
    loadBooksPage();
});

//list page button click event
document.getElementById("btn-readinglist").addEventListener("click", function() {
    loadListPage();
});